import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit, Output } from '@angular/core';
import * as EventEmitter from 'events';
import { DestinoViaje } from '../models/destino-viaje.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: DestinoViaje[];
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  constructor(private destinosApiClient: DestinoApiClient) {
    this.onItemAdded = new EventEmitter;
    this.destinos = []
    this.onItemAdded = new EventEmitter();
    this.updates= [];
    this.destinosApiClient.subscrbeOnChange((d:DestinoViaje) => {
      if (d != null){
        this.updates.push('Se ha elegido a'+ d.nombre);
      }
    });
  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje){
    this.destinosApiClient(d);
    this.onItemAdded.emit(d);
  }

   elegido(d: DestinoViaje){
     this.destinosApiClient.elegir(e);

   }

}
